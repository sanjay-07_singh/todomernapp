const express=require('express'),
router=express.Router(),
Todo=require('../../models/Todo');

router.get('/',(req,res)=>{
    Todo.find()
        .then(todo=>{
            if(!todo.length)
            return res.status(404).json({notodoyet:'No todos found yet'});
            res.status(200).json(todo);
        })
        .catch(err=>console.log(err));
});

router.post('/add',(req,res)=>{
    let todo={};
    todo=req.body;
    new Todo(todo).save()
                  .then(todo=>res.status(200).json(todo))
                  .catch(err=>console.log(err));
})

router.get('/:id',(req,res)=>{
    Todo.findOne({_id:req.params.id})
        .then(todo=>res.status(200).json(todo))
        .catch(err=>console.log(err));
})

router.post('/update/:id',(req,res)=>{
Todo.findOne({_id:req.params.id})
    .then(todo=>{
        if(!todo)
        return res.status(404).json({todonotfoundtoupdate:'Todo not found to update'});
        const todoValues={};
        todoValues.title=req.body.title;
        todoValues.name=req.body.name;
        todoValues.prior=req.body.prior;
        todoValues.isDone=req.body.isDone;
        Todo.findOneAndUpdate({_id:req.params.id},{$set:todoValues},{new:true})
            .then(todo=>res.status(200).json(todo))
            .catch(err=>console.log(err));
    })
    .catch(err=>console.length(err));
})

module.exports=router;
