import React,{Component} from 'react';
import axios from 'axios';

export default class EditTodos extends Component {
  constructor(props){
    super(props);
    this.state = {
      title:'',
      name:'',
      prior:'Low',
      isDone:false
    };
    this.desc=this.desc.bind(this);
    this.res=this.res.bind(this);
    this.pri=this.pri.bind(this);
    this.check=this.check.bind(this);
    this.onSubmit=this.onSubmit.bind(this);
  }
  componentDidMount(){
    axios.get(`https://thawing-retreat-97574.herokuapp.com/todo/${this.props.match.params.id}`)
         .then(res=>{
           this.setState({
             title:res.data.title,
             name:res.data.name,
             prior:res.data.prior,
             isDone:res.data.isDone
           });
         })
         .catch(err=>console.log(err));
  }
  onSubmit(event){
    event.preventDefault();
    let a={
      title:this.state.title,
      name:this.state.name,
      prior:this.state.prior,
      isDone:this.state.isDone,
    }
    axios.post(`https://thawing-retreat-97574.herokuapp.com/todo/update/${this.props.match.params.id}`,a)
         .then(res=>console.log(''))
         .catch(err=>console.log(err));
          this.props.history.push('/');
  }
  desc(event){
    this.setState({title:event.target.value.toUpperCase()});
  }
  res(event){
    this.setState({name:event.target.value.toUpperCase()});
  }
  pri(event){
    this.setState({prior:event.target.value});
  }
  check(event){
    this.setState({isDone:!this.state.isDone});
  }
  render(){
    return(
      <div className="m-5 p-5 c">
      <h1 className="text-light pt-4 display-5">Update Todo</h1>
      <form className="ct mt-5" onSubmit={this.onSubmit}>
  <div className="form-row">
    <div className="form-group col-12">
      <label>Description : </label>
      <input type="text" className="form-control" id="inputEmail4" placeholder="Enter your todo" onChange={this.desc} value={this.state.title}/>
    </div>
  </div>
  <div className="form-group mt-3">
    <label>Responsible : </label>
    <input type="text" className="form-control" id="inputAddress" placeholder="Enter your name" onChange={this.res}
    value={this.state.name}/>
  </div>
  <div className="form-check form-check-inline mt-3">
    <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="Low" onChange={this.pri} checked={this.state.prior==="Low"}/>
    <label className="form-check-label" > Low</label>
  </div>
  <div className="form-check form-check-inline px-4">
    <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="Medium" onChange={this.pri} checked={this.state.prior==="Medium"}/>
    <label className="form-check-label" > Medium</label>
  </div>
  <div className="form-check form-check-inline">
    <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="High" onChange={this.pri} checked={this.state.prior==="High"}/>
    <label className="form-check-label" > High</label>
  </div>
  <div className="form-check mt-5">
    <input  className="form-check-input"
            id="completedCheckbox"
            type="checkbox"
            name="completedCheckbox"
            onChange={this.check}
            checked={this.state.isDone}
            value={this.state.isDone}
            />
          <label className="form-check-label ml-1" htmlFor="completedCheckbox">
        Completed
    </label>
</div>
  <br/>
  <br/>
  <button type="submit" className="btn btn-info btn-block mt-4 text-uppercase"><i className="fa fa-pencil" aria-hidden="true"></i> Update your todo</button>
</form>
      </div>
    );
  }
}
