import React,{Component} from 'react';
import axios from 'axios';
import Table from './Table';

export default class TodosList extends Component {
  todoList(){
    return this.state.todos.map((a,i)=>
  <Table todo={a} key={i}/>);
  }
  componentDidMount(){
    axios.get('https://thawing-retreat-97574.herokuapp.com/todo/')
         .then(res=>{
           this.setState({todos:res.data});
         })
         .catch(err=>console.log(err));
  }
  componentDidUpdate(){
    axios.get('https://thawing-retreat-97574.herokuapp.com/todo/')
         .then(res=>{
           this.setState({todos:res.data});
         })
         .catch(err=>console.log(err));
  }
  constructor(props){
    super(props);
    this.state = {
      todos:[]
    };
  }
  render(){
    return(
      <div>
        <h3 className="text-uppercase text-center mt-5 pt-5">Todos List</h3>
          <table className="table table-bordered table-striped mt-5 pt-5">
    <thead>
      <tr>
        <th scope="col">Description</th>
        <th scope="col">Name</th>
        <th scope="col">Priority</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      {this.todoList()}
    </tbody>
  </table>
      </div>
    );
  }
}
