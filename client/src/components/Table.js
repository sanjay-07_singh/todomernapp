import React,{Component} from 'react';
import {Link} from 'react-router-dom';

export default class Table extends Component {
  render(){
    return(
      <tr>
      <td className={this.props.todo.isDone?'comp':''}>{this.props.todo.title}</td>
      <td className={this.props.todo.isDone?'comp':''}>{this.props.todo.name}</td>
      <td className={this.props.todo.isDone?'comp':''}>{this.props.todo.prior}</td>
      <td><Link to={`/edit/${this.props.todo._id}`}>Edit</Link></td>
    </tr>
    );
  }
}
