import React,{Component} from 'react';
import {BrowserRouter as Router,Route,Link} from 'react-router-dom';
import TodosList from './components/TodosList';
import EditTodos from './components/EditTodos';
import Create from './components/Create';

export default class App extends Component {
  render(){
    return(
      <Router>
        <div>
          <nav className="navbar navbar-expand navbar-dark bg-danger w-100">
  <a className="navbar-brand text-uppercase text-light d-none d-md-inline"
  href="https://sanjay072000.github.io/newFrontEnd/Portfolio/" target="_blank">
 <img src="./images/download.png" width="30" height="30" alt="Unavailable"
 className="img-fluid mr-3"/>
  </a>
  <Link to="/" className="navbar-brand"> TODO APP</Link>
  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav d-flex justify-content-end w-100">
      <li className="nav-item active mr-5">
        <Link to="/" className="nav-link"><i className="fa fa-id-card-o" aria-hidden="true"></i> Todos-List <span className="sr-only">(current)</span></Link>
      </li>
      <li className="nav-item active">
        <Link to="/create" className="nav-link"><i className="fa fa-pencil" aria-hidden="true"></i> Create-Todo</Link>
      </li>
    </ul>
  </div>
</nav>
      <Route path='/' exact component={TodosList}/>
      <Route path='/edit/:id' component={EditTodos}/>
      <Route path='/create' component={Create}/>
      </div>
      </Router>

    );
  }
}
