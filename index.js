const express=require('express'),
app=express(),
bodyparser=require('body-parser'),
cors=require('cors'),
port=process.env.PORT || 3000,
dbstr=require('./setup/mongourl').mongourl,
mongoose=require('mongoose'),
todo=require('./routes/api/todo'),
path=require('path');



mongoose.connect(dbstr,{useNewUrlParser:true})
        .then(()=>console.log('Mongodb connected successfully'))
        .catch(err=>console.log(err));


app.use(bodyparser.urlencoded({extended:true}));
app.use(bodyparser.json());
app.use(cors());
app.use('/todo',todo);

// serve static assets if in production
if(process.env.NODE_ENV==="production"){
        // set static folder
        app.use(express.static('client/build'));
        app.get('*',(req,res)=>{
             res.sendFile(path.resolve(__dirname,'client','build','index.html'));   
        })
        }

app.listen(port,()=>console.log(`Server is running at port ${port}`));


