const mongoose=require('mongoose'),
Schema=mongoose.Schema,
todoSchema=new Schema({
title:{
    type:String,
},
name:{
    type:String
},
prior:{
    type:String
},
isDone:{
    type:Boolean,
    default:false
}
});

module.exports=mongoose.model('todo',todoSchema);

